/*
  test program to load, relocate and run executables

  you need to disable the actual call to the executable when
  compiling for non-Amiga platforms-
*/
#include "hunkload.h"
#include "lists.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main( int argc, char **argv )
{
	struct hunkload *infile;
	long   errcode;
	int i;

	if( argc < 2 )
	{
		printf("usage: %s file1 <file2> <file...>\n",argv[0]);
		return 1;
	}

	for( i=1 ; i < argc ; i++ )
	{

		infile = hunk_load( argv[1], 0, &errcode );
		if( infile )
		{
			printf("loaded hunk file, total size %ld\n",(long)infile->totalsize);
		}
		else
			printf("error loading input file, error code %ld\n",errcode);

		/* get reloc statistics */

		/* TODO */

		/* test relocation */
		if( infile )
		{
			long res;
			unsigned char*mem = malloc( infile->totalsize );

			res = hunk_relocate( infile, mem, mem, 0 );
			printf("relocate result %ld\n",res);
#if 0
			if( res == HLERR_OK )
			{
			 int (*myfunc)( int argc, char **argv);
			 myfunc = (int (*)( int, char **))mem;
			 myfunc(argc,argv);
			}
#endif
			free( mem );
		}

		hunk_free( infile );
	}

	/* print reloc statistics */

	/* TODO */

	return 0;
}
