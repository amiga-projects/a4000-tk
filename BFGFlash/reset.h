/*
; simple reset code
; (C) 2022 Henryk Richter
;------------------------------------------------------------------------------
*/
#ifndef _INC_RESET_H
#define _INC_RESET_H

#include "compiler.h"

ASM void KBDReset( void );

ASM void KBDR_Blink( void );

#endif /* _INC_RESET_H */
