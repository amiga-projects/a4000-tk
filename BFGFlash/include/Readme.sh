#!/bin/bash
# This directory contains the includes for MMULib, augmented by files that
# are suitable for GCC and VBCC, provided by this Script
#
# Please note that these additional files are neither part of the MMULib
# distribution, nor widely tested. It is highly suggested to download the
# latest official distribution from http://aminet.net/package/docs/misc/MuManual.lha 
# for personal projects

# gcc
fd2pragma -i fd/mmu_lib.fd -c clib/mmu_protos.h -s 47 --to inline/
fd2pragma -i fd/mmu_lib.fd -c clib/mmu_protos.h -s 35 --to proto/

fd2pragma -i fd/68040_lib.fd -c clib/68040_protos.h -s 47 --to inline/
fd2pragma -i fd/68040_lib.fd -c clib/68040_protos.h -s 35 --to proto/

fd2pragma -i fd/68060_lib.fd -c clib/68060_protos.h -s 47 --to inline/
fd2pragma -i fd/68060_lib.fd -c clib/68060_protos.h -s 35 --to proto/

fd2pragma -i fd/680x0_lib.fd -c clib/680x0_protos.h -s 47 --to inline/
fd2pragma -i fd/680x0_lib.fd -c clib/680x0_protos.h -s 35 --to proto/

fd2pragma -i fd/disassembler_lib.fd -c clib/disassembler_protos.h -s 47 --to inline/
fd2pragma -i fd/disassembler_lib.fd -c clib/disassembler_protos.h -s 35 --to proto/



# vbcc
fd2pragma -s 38 --voidbase -i fd/mmu_lib.fd --clib clib/mmu_protos.h --to vbcc/proto/
fd2pragma --special 70 --voidbase --infile fd/mmu_lib.fd -c clib/mmu_protos.h --to vbcc/inline
