#ifndef _PROTO_MMU_H
#define _PROTO_MMU_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_MMU_PROTOS_H) && !defined(__GNUC__)
#include <clib/mmu_protos.h>
#endif

#ifndef __NOLIBBASE__
extern struct Library *MMUBase;
#endif

#ifdef __GNUC__
#ifdef __AROS__
#include <defines/mmu.h>
#else
#include <inline/mmu.h>
#endif
#elif !defined(__VBCC__)
#include <pragma/mmu_lib.h>
#endif

#endif	/*  _PROTO_MMU_H  */
