#ifndef _PROTO_MC68060_H
#define _PROTO_MC68060_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_MC68060_PROTOS_H) && !defined(__GNUC__)
#include <clib/mc68060_protos.h>
#endif

#ifndef __NOLIBBASE__
extern struct Library *MC68060Base;
#endif

#ifdef __GNUC__
#ifdef __AROS__
#include <defines/mc68060.h>
#else
#include <inline/mc68060.h>
#endif
#elif !defined(__VBCC__)
#include <pragma/mc68060_lib.h>
#endif

#endif	/*  _PROTO_MC68060_H  */
