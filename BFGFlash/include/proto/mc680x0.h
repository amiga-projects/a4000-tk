#ifndef _PROTO_MC680X0_H
#define _PROTO_MC680X0_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_MC680X0_PROTOS_H) && !defined(__GNUC__)
#include <clib/mc680x0_protos.h>
#endif

#ifndef __NOLIBBASE__
extern struct Library *MC680x0Base;
#endif

#ifdef __GNUC__
#ifdef __AROS__
#include <defines/mc680x0.h>
#else
#include <inline/mc680x0.h>
#endif
#elif !defined(__VBCC__)
#include <pragma/mc680x0_lib.h>
#endif

#endif	/*  _PROTO_MC680X0_H  */
