/*
  FlashTest
  (C) 2022 Henryk Richter

  Check if flash erasing/writing works on BFG9060

  Synopsis (i.e. no args):
   FlashTest

*/
#include "flash.h"
#include "hunkload.h"
#include "mmustuff.h"
#include <dos/dos.h>
#include <exec/memory.h>
#include <exec/execbase.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/mmu.h>
#include <mmu/context.h>
#include <mmu/mmutags.h>

#ifndef AFF_68060
#define AFF_68060 (1<<7)
#endif

struct Library *MMUBase;

/* count (backwards) the number of uninitialized bytes */
long empty_area( struct flashrw *fsh );

#define TEST_SIZE 8
unsigned char TEST_PATTERN[TEST_SIZE] = { 0x55, 0xaa,0x55,0xaa,0x55,0xaa,0x55,0xaa };


void print_flashtype( struct flashrw *fsh);
STRPTR flash_errmsg( long errcode );

int main( int argc, char **argv )
{
  struct ExecBase *SysBase = *(struct ExecBase**)0x4;
  long errcode = HLERR_OK;
  long free_bytes=0,test_off,test_bytes;
  struct flashrw flashhandle;

  if( !(SysBase->AttnFlags & (AFF_68040|AFF_68060) ))
  {
	Printf( (STRPTR)"This program requires a 68040 or 68060 CPU. Exiting.\n");
	return 5;
  }

  MMUBase = OpenLibrary( (STRPTR)"mmu.library", 46 );
  if( MMUBase )
  {
	struct MMUContext *ctx,*sctx;
	ULONG props;
	  
	ctx=DefaultContext();
	sctx=SuperContext(ctx);

	props  = GetProperties(sctx,(ULONG)FLASH_DEFAULT,TAG_DONE);
	props |= GetProperties(sctx,(ULONG)FLASH_DEFAULT,TAG_DONE);

	if( (props & MAPP_REMAPPED) )
	{
		Printf( (STRPTR)"Alert! Flash is remapped elsewhere by MMU. Cannot access Flash chip directly\n");
	}
  
  	if( props & (MAPP_COPYBACK|MAPP_IMPRECISE|MAPP_ROM) )
  	{
  		Printf( (STRPTR)"Caution: MMU is not set up properly for Flash access, property flags %lx\n",props);
  		Printf( (STRPTR)"Execute the following line from Shell:\nmusetcachemode from 0xf00000 size=0x80000 cacheinhibit\n");
  	}
  	else	Printf( (STRPTR)"Suitable MMU setup assumed\n");
 
  }

  /* init flash, get size and check writable flags */
  if( errcode == HLERR_OK )
  {
	errcode = flashrw_init( &flashhandle );
	Printf( (STRPTR)"Found flash device:\n");
	print_flashtype(&flashhandle);

	if( errcode != FRWERR_OK )
	{
		Printf( (STRPTR)"Flash init failed, error code %ld = %s\n",errcode,(ULONG)flash_errmsg(errcode));
		if(errcode == -2){
			Printf( (STRPTR)"Please check your mmu-configuration:\nMake sure 0x00F00000 is writable (not \"ROM\")!\n");
		}
		errcode = HLERR_WRONGARG;
	}
	else
	{
		Printf( (STRPTR)" with %ld Bytes capacity detected.\n",(LONG)flashhandle.frw_TotalBytes);
	}
  }

  /* see how much space there is in the flash */
  if( errcode == HLERR_OK )
  {
	free_bytes = empty_area( &flashhandle );
	if( free_bytes < flashhandle.frw_SectorSize )
	{
		Printf( (STRPTR)"Error: %ld bytes unused in Flash, but need %ld for testing\n",(LONG)free_bytes,(LONG)flashhandle.frw_SectorSize);
		errcode = HLERR_WRONGARG;
	}
	else
	{
		Printf( (STRPTR)"OK. %ld bytes free in Flash for testing\n",(LONG)free_bytes);
	}
  }

  if( errcode == HLERR_OK )
  {
	long off;

	test_bytes = free_bytes & ~(flashhandle.frw_SectorSize-1); /* align space to sector boundary, see above: free_bytes>=flashhandle.frw_SectorSize */
	test_off   = flashhandle.frw_TotalBytes - test_bytes; /* offset in flash, aligned to sector boundary */

	/* first: write something in all free sectors */
	off = test_off;
	Printf( (STRPTR)"Writing to offset ");
	while( off < flashhandle.frw_TotalBytes )
	{
		Printf( (STRPTR)"%ld ",off );
		errcode = flashrw_write( &flashhandle, off, TEST_SIZE, TEST_PATTERN ); 
		if( errcode != FRWERR_OK )
		{
			Printf( (STRPTR)"- Write error, code %ld.",errcode);
			break;
		}

		off += flashhandle.frw_SectorSize;
	}
	Printf( (STRPTR)"\n");

	/* now erase what we wrote */
	Printf( (STRPTR)"Erasing free sectors again... ");
	errcode = flashrw_erase( &flashhandle, test_off, test_bytes );
	if( errcode != 0 )
	{
		Printf( (STRPTR)"Error, code %ld\n",errcode);
	}
	else	Printf( (STRPTR)"Done." );

	/* final verdict */
	if( errcode == 0 )
	{
		if( free_bytes != empty_area( &flashhandle ) )
		{
			Printf( (STRPTR)"Empty area does not match: erasing failed\n");
		}
		else	Printf( (STRPTR)"OK. Empty space is empty again (as intended)\n");
	}
  }

  Printf( (STRPTR)"Done.\n");

  CloseLibrary( MMUBase );

  return 0;
}

void print_flashtype( struct flashrw *fsh)
{

 switch( fsh->frw_Manufacturer )
 {
	case 0x1: Printf( (STRPTR)"AM");break; /* also: TI, Spansion, FT */
	case 0x20: Printf( (STRPTR)"ST");break;
	case 0xC2: Printf( (STRPTR)"MX");break;
	case 0xBF: Printf( (STRPTR)"SST" );break;
	default: Printf( (STRPTR)"unkown manufactor: 0x%x\n",fsh->frw_Manufacturer);break;
 }

 switch( fsh->frw_Model )
 {
	case 0x20: Printf( (STRPTR)"29F010");break;
	case 0xA4: Printf( (STRPTR)"29F040");break;
	case 0xB5: Printf( (STRPTR)"39SF010" );break;
	case 0xB6: Printf( (STRPTR)"39SF020" );break;
	case 0xB7: Printf( (STRPTR)"39SF040" );break;
	default: Printf( (STRPTR)"unknown model: 0x%x\n",fsh->frw_Model);break;
 }

}

STRPTR ErrStrings[8] = {
 (STRPTR)"OK", /* no problem */
 (STRPTR)"Parameter problem",
 (STRPTR)"Unknown flash",
 (STRPTR)"Sector write protection",
 (STRPTR)"Erase overflow",
 (STRPTR)"Write error",
 (STRPTR)"Unspecified error",
 (STRPTR)"$VER: FlashBFGCLI 0.2 (15.7.22) by Henryk Richter"
};

STRPTR flash_errmsg( long errcode )
{
	if( (errcode < -6) || (errcode > 0) )
		errcode = -6;

	return ErrStrings[-errcode];
}

long empty_area( struct flashrw *fsh )
{
	long ret = 0;
	unsigned char *bp = (unsigned char *)fsh->frw_Base;
	unsigned char *p  = bp + fsh->frw_TotalBytes;
	
	while( --p >= bp )
	{
		if( *p != 0xff )
			break;
		ret++;
	}

	return ret;
}


