; dummy ROM header that leaves 2k free in front to be overwritten
; by boot code
; (C)2020 Henryk Richter, CC BY SA
;
; vasm -Fhunkexe -o romheader_2kspace romheader_2kspace.asm 
;
; Concept:
; - set up a 512k F0 ROM in Remus, put this in front and 
;   add the modules you like (e.g. workbench.library, icon.library etc.)
; - compile the F0 ROM and overwrite the first 2k with custom
;   boot code for 060 cards
; - flash result to obtain a Boot ROM as well as custom kickstart modules
;

SPACE	EQU	2048
 ;-- Example Rom Header asm for Remus.

Version = 45
Revision= 10

 DATA

RomStart:
  dc.w $1114    ;-- ROM Identifier
  dc.w $4ef9    ; 'JMP'
  dc.l $F80002  ;-- Jumps to the start of the normal kickstart
                ;-- ..which is a JMP <start address>

;-- this isn't really needed...
  dc.w 0
  dc.w $FFFF

  dc.w Version      ; Rom version
  dc.w Revision      ; Rom revision .. 45.10
RomEnd:

  ;dummy ROMTAG
.r
  dc.w	$4AFC
  dc.l	.r
  dc.l	DummyEnd
  dc.b    2               ;RTF_SINGLETASK
  dc.b    40              ;Version
  dc.b    0               ;Type
  dc.b    0               ;Priority (<exec_pri, hence ignored)
  dc.l    .dummyname
  dc.l    .dummyname
  dc.l    .rts_func
.rts_func:
	rts
.dummyname:
  dc.b	"placeholder_module_no_function"

  dcb.b	SPACE-*-RomStart,$ff
DummyEnd:
End
