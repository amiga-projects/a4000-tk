----------------------------------------------------------------------------------
-- Company: A1K
-- Engineer: Matthias Heinrichs
-- 
-- Create Date:    09:00  2020/01/13
-- Design Name:    SDRAM-Controller with 68060 Line burst support
-- Module Name:    ramcon - Behavioral 
-- Project Name:   A4000-TK
-- Target Devices: XC95288XL-TQFP144-10ns
-- Revision: 0.1
-- Additional Comments: 
--
-- SET TABS TO FOUR!
----------------------------------------------------------------------------------

Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
entity ramcon is
	Port (
		CLK_RAMC : in  STD_LOGIC;		
		RESET : in std_logic;
		C25MHZ : in std_logic;
		RW_40 : in std_logic;
		TT40_1 : in std_logic;
		TS40: in std_logic;
		AS30: in std_logic;
		A40		: in STD_LOGIC_VECTOR (31 downto 0);
		SIZ40		: in STD_LOGIC_VECTOR (1 downto 0);
		DQ: out STD_LOGIC_VECTOR (3 downto 0);
		WE: out std_logic;
		CAS: out std_logic;
		RAS: out std_logic;
		CLK_RAM: out std_logic;
		CLKEN: out std_logic:= '1';
		LE_RAM: out std_logic;
		OERAM_40: out std_logic;
		OE40_RAM: out std_logic;
		CE 		: out STD_LOGIC_VECTOR (1 downto 0);      
		BA			: out STD_LOGIC_VECTOR (1 downto 0);      
		ARAM		: out STD_LOGIC_VECTOR (12 downto 0);      
		RAM_ACCESS: out std_logic;
		CACHE_INHIBIT: out std_logic;
		TA_RAM: out std_logic
	);
end ramcon;


architecture ramcon_behav of ramcon is
	TYPE sdram_state_machine_type IS (
				powerup,
				init_precharge,
				init_precharge_commit,
				init_opcode,			
				start_state,	
				refresh_start,	
				refresh_wait,	
				commit_ras,
				read_start_cas,
				read_wait_cas,	
				read_data_wait,		
				write_start_cas,	
				write_commit_cas,
				write_recover,
				precharge,				
				end_cycle
				);
				
	constant refresh_cycles : integer := 3;
	constant cs_bit : integer := 26;
	constant ram_a12_bit : integer := 25;
				 

	signal TRANSFER_ACLR: std_logic;
	signal TRANSFER_CLK: std_logic;
	signal REFRESH: std_logic;
	signal TRANSFER: std_logic;
	signal TA40_FB: std_logic;
	signal NQ :  STD_LOGIC_VECTOR (1 downto 0);

	signal RQ :  STD_LOGIC_VECTOR (7 downto 0);
	
	-- Acceptable values for this in ISE are: one-hot compact speed1 sequential johnson gray and auto.
	-- Acceptable values for this in vivado are: one_hot sequential johnson gray and auto.
	-- NOTE THE DIFFERENT SPELLING OF "one hot"
	signal CQ :  sdram_state_machine_type := powerup;
	attribute fsm_encoding : string;
	attribute fsm_encoding of CQ  : signal is "gray";

	signal SELRAM :  STD_LOGIC;     
	signal ARAM_LOW: STD_LOGIC_VECTOR (12 downto 0);      
	signal ARAM_HIGH: STD_LOGIC_VECTOR (12 downto 0);      
	constant ARAM_OPTCODE: STD_LOGIC_VECTOR (12 downto 0) := "0000000100010"; 
	--constant ARAM_OPTCODE: STD_LOGIC_VECTOR (12 downto 0) := "0000000100000"; --no burst
	
	signal RAM_READY : STD_LOGIC;
	signal BURST :  STD_LOGIC_VECTOR (1 downto 0);
	signal BURST_ACCESS_R : STD_LOGIC;
	signal BYTE_ENCODE :  STD_LOGIC_VECTOR (3 downto 0);
	signal LE_RAM_SIG : STD_LOGIC;
	signal DMA_CYCLE : STD_LOGIC;
	signal RESET_D0 : STD_LOGIC;
	
	Function to_std_logic(X: in Boolean) return Std_Logic is
	variable ret : std_logic;
	begin
	if x then ret := '1';  else ret := '0'; end if;
	return ret;
	end to_std_logic;


	-- sizeIt replicates a value to an array of specific length.
	Function sizeIt(a: std_Logic; len: integer) return std_logic_vector is
		variable rep: std_logic_vector( len-1 downto 0);
	begin for i in rep'range loop rep(i) := a;  end loop; return rep;
	end sizeIt;
begin
	
	-- Signal section
				
	--output signals
	TA_RAM 		<= TA40_FB;-- when (SELRAM='1') else '1';
	CLK_RAM <=  CLK_RAMC;
	RAM_ACCESS 	<= not SELRAM;

	-- helper signals
	SELRAM	<= '1' when A40(31 downto 27) = "00001" else '0'; 
	TRANSFER_CLK <= '1' when TS40 ='0' and SELRAM='1' and TT40_1 ='0' else '0';
	TRANSFER_ACLR <= '1' when 	CQ = commit_ras or
										RESET ='0' else '0';
										
	ARAM_LOW <= std_logic_vector'("000" & A40(ram_a12_bit) & A40(10 downto 2));
	ARAM_HIGH <= A40(ram_a12_bit) &A40(22 downto 11);
	LE_RAM <= LE_RAM_SIG;

	-- Register Section

	RAM_STATES:process (CLK_RAMC) begin
		if falling_edge(CLK_RAMC) then	
			RESET_D0 <= RESET;
			--refresh
			if (CQ= refresh_start)	then
				REFRESH <= '0';
			elsif(RQ >= "10100000") then
				REFRESH <= '1';
			end if;
						
					
			--RAM state helpers
			if(	CQ = refresh_wait)then
				NQ <= NQ +1;
			else 
				NQ  <= (others => '0');
			end if;

			if (RESET_D0 = '0' and RESET = '1') then
				RAM_READY <= '0';
			elsif(CQ = refresh_wait and NQ >= refresh_cycles) then
				RAM_READY <= '1';
			end if;
			
			--burst control			
			if(CQ = commit_ras) then
				if( SIZ40 = "11" --long word 
					and DMA_CYCLE = '1'--no DMA-cycle running
					)then 
					--Write: burst of 3
					--Read: burst of 4
					BURST <= ('1' & RW_40);
					BURST_ACCESS_R <= '1';
				else
					BURST <="00"; --no burst
					BURST_ACCESS_R <= not RW_40;
				end if;
			elsif(CQ = read_data_wait or CQ = write_commit_cas) then
				BURST <= BURST -1; --decrement
			elsif(CQ=end_cycle)then
				BURST_ACCESS_R <= '1';
			end if;	
		
			--RAM statemachine
			--default values
			WE <= '1';
			CAS <= '1';
			RAS <= '1';
			CLKEN <= '1';
			ARAM <= ARAM_HIGH;
			CE <=  ( A40(cs_bit) & not A40(cs_bit));
			BA <= A40(24 downto 23);	
			DQ <= "1111";
			case CQ is
			when powerup =>
				if(RESET = '1') then
					CQ <= init_precharge;
				end if;
			when init_precharge =>
				CE <= "00";
				ARAM(10) <= '1'; --precharge all
				WE <= '0';
				RAS <= '0';
				CQ <= init_precharge_commit;
			when init_precharge_commit =>
				CQ <= init_opcode;
			when init_opcode =>
				CE <= "00";
				BA <= "00";
				WE <= '0';
				CAS <= '0';
				RAS <= '0';
				ARAM <= ARAM_OPTCODE;
				CQ <= refresh_start;
			when start_state =>
				if (REFRESH='1') then
					CQ <= refresh_start;
				elsif ( TRANSFER ='1'
						) then
					RAS <= '0';
					CQ <= commit_ras;
				elsif(RAM_READY ='0') then
					CQ <= powerup;
				else				
					CQ <= start_state;
				end if;
			when refresh_start =>
				CE <= "00";
				CAS <= '0';
				RAS <= '0';
				CQ <= refresh_wait;
			when refresh_wait =>
				if (NQ = refresh_cycles) then
					if(RAM_READY = '1')then -- this trick triggers the 2nd refresh after power up
						CQ <= start_state;
					else
						CQ <= refresh_start;
					end if;
				else
					CQ <= refresh_wait;
				end if;
			when commit_ras =>	
				if(RW_40='1')then
					CQ <= read_start_cas;
				else
					CQ <= write_start_cas;
				end if;
			when read_start_cas =>
				CAS <= '0';
				ARAM <= ARAM_LOW;
				ARAM(10) <= BURST(1); -- autoprecharge on full burst
				DQ <= "0000";
				if(BURST(1) = '1')then --burst
					CQ <= read_wait_cas;
				else
					CQ <= precharge;
				end if;
			when read_wait_cas =>
				DQ <= "0000";
				CQ <= read_data_wait;
			when read_data_wait =>
				DQ <= "0000";
				if (BURST ="00") then
					CQ <= end_cycle; --yes, we can stop here due to autoprecharge!
				else				
					CQ <= read_data_wait;
				end if;
			when write_start_cas =>
				WE <= '0';
				CAS <= '0';
				ARAM <= ARAM_LOW;
				--ARAM(10) <= BURST(1); -- autoprecharge on full burst
				DQ <= BYTE_ENCODE;
				if(BURST(1) = '1')then --burst
					CQ <= write_commit_cas;
				else
					CQ <= write_recover;
				end if;
			when write_commit_cas =>			
				DQ <= BYTE_ENCODE;
				if (BURST ="00") then
					CQ <= precharge;
				else
					CQ <= write_commit_cas;					
				end if;	
			when write_recover =>
				WE <= '0';
				CQ <= precharge;
			when precharge =>
				ARAM(10) <= '1'; --precharge all
				CE <= "00"; 
				WE <= '0';
				RAS <= '0';
				CQ <= end_cycle;
			when end_cycle =>
				CQ <= start_state;		
			end case;
		end if;
	end process;
	
	REFRESH_COUNTER:process(RESET,CQ,C25MHZ)begin
		if (CQ= refresh_start)	then
			RQ<=	"00000000";
		elsif falling_edge(C25MHZ) then
			RQ <= RQ +1;
		end if;
	end process;
	
	process(TRANSFER_ACLR,CLK_RAMC)begin
		if(TRANSFER_ACLR='1') then
			TRANSFER <= '0';
		elsif(rising_edge(CLK_RAMC))then	
			if(TRANSFER_CLK ='1')then
				TRANSFER <= '1';
			end if;
		end if;
	end process;

	TA_GEN:process(CLK_RAMC,RESET)begin
		if(RESET ='0' )then
			TA40_FB <= '1'; 
		elsif falling_edge(CLK_RAMC)then
		
			--this signal is prepared for the NEXT Clock!
			if( (CQ = end_cycle and BURST_ACCESS_R = '0') or -- read acknowlege for a single non-burst long 
				CQ = read_data_wait or -- read acknowlege for burst long				
				CQ = write_start_cas or -- write acknowlege for first long (burst and non burst)
				CQ = write_commit_cas -- write acknowlege for burst long	
				)then
				TA40_FB <= '0';
			else
				TA40_FB <= '1';
			end if;
			
		end if;
	end process;
	
	SIGNALS:process(CLK_RAMC,RESET,RAM_READY)begin
		if(RESET ='0' or RAM_READY='0')then
			BYTE_ENCODE <= "1111";			
			LE_RAM_SIG <= '1';
			--TA40_FB <= '1'; 
			CACHE_INHIBIT <= '1';
			OERAM_40	<='1';
			OE40_RAM	<='1';
			DMA_CYCLE	<='1';

		elsif rising_edge(CLK_RAMC)then

			if(TRANSFER='1')	then
				OERAM_40	<= not RW_40;
				OE40_RAM 	<= RW_40;
				DMA_CYCLE	<= AS30;
			elsif(CQ = start_state and DMA_CYCLE ='1')then
				OERAM_40	<='1';
				OE40_RAM	<='1';
			elsif(DMA_CYCLE ='0' and AS30='1')then
				OERAM_40	<='1';
				OE40_RAM	<='1';
				DMA_CYCLE	<='1';
			end if;
		
			--cache signal control
			if(	SELRAM= '1'  
				or (A40(31 downto 27) = "00000" and (A40(26) = '1' or A40(25) = '1' or A40(24) = '1')) 		--$04000000-$07FFFFFF mobo fast
				or (A40(31) = '0' and (A40(30) = '1' or A40(29) = '1' or A40(28) = '1'))		--$10000000-$7FFFFFFF Z3-Space								
				or (A40(31 downto 20) = (x"00F")) --Boot ROM
			)then
				CACHE_INHIBIT <= '0';
			else
				CACHE_INHIBIT <= '1';
			end if;		
		
			--this signal is prepared for the NEXT Clock!
			if( (CQ = end_cycle and BURST_ACCESS_R = '0') or -- read acknowlege for a single non-burst long 
				CQ = read_data_wait 
				)then
				
				LE_RAM_SIG <= '0';
			else
				LE_RAM_SIG <= '1';
			end if;
			
			--sizing
--			if(CQ = commit_ras) then
				if(	--RW_40 ='1' or 
					SIZ40 = "00" or SIZ40 = "11") then
					BYTE_ENCODE <= "0000";
				else
					if(SIZ40 = "01")then
						case (A40 (1 downto 0)) is
							when "00" => BYTE_ENCODE <="1110";
							when "01" => BYTE_ENCODE <="1101";
							when "10" => BYTE_ENCODE <="1011";
							when "11" => BYTE_ENCODE <="0111";
							when others => BYTE_ENCODE <="1110"; -- somehow Xilinx ISE does not see that case is complete
						end case;
					else
						if(A40(1) = '0')then
		 					BYTE_ENCODE <="1100";
						else
							BYTE_ENCODE <="0011";
						end if;
					end if;
				end if;
--			elsif(CQ = end_cycle or CQ = write_recover or CQ = precharge)then
--				BYTE_ENCODE <= "1111";
--			end if;

		end if;
	end process;

end ramcon_behav;
