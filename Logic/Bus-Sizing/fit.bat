SET PATH=%PATH%;c:\Xilinx_ISE\14.7\ISE_DS\ISE\bin\nt64;c:\Xilinx_ISE\14.7\ISE_DS\ISE\lib\nt64

cpldfit.exe -intstyle ise -p xc9572xl-10-TQ100 -ofmt vhdl -htmlrpt -optimize density -loc on -slew fast -init low -inputs %1 -pterms %2 -unused float -power std -terminate float bus_sizing.ngd >log.txt 
@IF %ERRORLEVEL% NEQ 0 GOTO NO_SUCCESS
tsim -intstyle ise bus_sizing bus_sizing.nga
hprep6 -s IEEE1149 -n bus_sizing -i bus_sizing
GOTO END
:NO_SUCCESS
@echo !!!!
@echo error fitting the jed!
@echo !!!!
:END