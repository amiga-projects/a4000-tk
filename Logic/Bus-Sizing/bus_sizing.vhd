----------------------------------------------------------------------------------
-- Company: A1K
-- Engineer: Matthias Heinrichs
-- 
-- Create Date:    09:00  2020/01/13
-- Design Name:    68030-68060 Bus sizing controller
-- Module Name:    bus_sizing - Behavioral 
-- Project Name:   A4000-TK
-- Target Devices: XC9572XL-TQFP100-10ns
-- Revision: 0.1
-- Additional Comments: 
--
-- SET TABS TO FOUR!
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bus_sizing is
    Port ( RW030 : in  STD_LOGIC;
           BGACK030 : in  STD_LOGIC;
           BWL_BS : in  STD_LOGIC_VECTOR (2 downto 0);
           LE_BS : in  STD_LOGIC;
           OE_BS : in  STD_LOGIC;
           RESET30 : in  STD_LOGIC;
           D30 : inout  STD_LOGIC_VECTOR (31 downto 0);
           D40 : inout  STD_LOGIC_VECTOR (31 downto 0));
end bus_sizing;

architecture Behavioral of bus_sizing is

signal D40OUT: STD_LOGIC_VECTOR(31 downto 0):= (others =>'0');
signal D30OUT: STD_LOGIC_VECTOR(31 downto 0):= (others =>'0');
signal D30_SIG: STD_LOGIC_VECTOR(31 downto 0):= (others =>'0');

signal DIR_BS: STD_LOGIC:='0';

begin
	
	DIR_BS <= '1' when (RW030 ='1' and BGACK030 = '1') or
					   (RW030 ='0' and BGACK030 = '0') or
					   RESET30='0'
				  else '0';
	
	--oe ansteuerung f�r 040-bus (READ)
	D40	<=	(others =>'Z') when OE_BS='0' or DIR_BS = '0' or RESET30 ='0' else
			D40OUT ;--when BGACK030 = '1' else D30;

	--oe ansteuerung f�r 030-bus (WRITE) 
	D30	<=	(others =>'Z') when OE_BS='0' or DIR_BS = '1' or RESET30 ='0' else
			D30OUT ;--when BGACK030 = '1' else	D40;

	--I need the magic busmirroring feature on SOME byte accesses to fix the Bug in the A3000 SuperDMAC Rev2
	-- BWL_BS(2) ='1' indicates a BYTE-Write access

	--Zuordnung Datenleitungen von 040-BUS zu 030-BUS (WRITE)
	D30_SIG(31 downto 24)	<=	D40(31 downto 24) when BWL_BS (1 downto 0 ) = "00" else
								D40(23 downto 16) when BWL_BS (1 downto 0 ) = "01" else
								D40(15 downto  8) when BWL_BS (1 downto 0 ) = "10" else
								D40( 7 downto  0);
	D30_SIG(23 downto 16)	<=	--D40(31 downto 24) when BWL_BS (2 downto 0 ) = "100" else	-- do magic bus mirroring on byte access like a 68030! 
								--D40(23 downto 16) when BWL_BS (2 downto 0 ) = "101" else	-- do magic bus mirroring on byte access like a 68030!
								--D40(23 downto 16) when BWL_BS (2 downto 1 ) = "00" else		
								--D40(15 downto  8) when BWL_BS (2 downto 0 ) = "110" else	-- do magic bus mirroring on byte access like a 68030! 
								D40(23 downto 16) when BWL_BS (1 ) = '0' else		
								D40( 7 downto  0);											
	D30_SIG(15 downto  8)	<=	--D40(31 downto 24) when BWL_BS (2 downto 0 ) = "100" else	-- do magic bus mirroring on byte access like a 68030! 
								--D40(23 downto 16) when BWL_BS (2 downto 0 ) = "101" else	-- do magic bus mirroring on byte access like a 68030! 
								--D40(15 downto  8) when BWL_BS (2 downto 0 ) = "110" else	-- do magic bus mirroring on byte access like a 68030! 
								D40(15 downto  8);-- when BWL_BS (2) = '0' else								
								--D40( 7 downto  0);	
	D30_SIG( 7 downto  0)	<=	--D40(31 downto 24) when BWL_BS (2 downto 0 ) = "100" else	-- do magic bus mirroring on byte access like a 68030! 
								D40(23 downto 16) when BWL_BS (2 downto 0 ) = "101" else	-- do magic bus mirroring on byte access like a 68030!
								--D40(15 downto  8) when BWL_BS (2 downto 0 ) = "110" else	-- do magic bus mirroring on byte access like a 68030! 
								D40( 7 downto  0);	
	--latch for write
	LATCH_40_W: process (LE_BS, RESET30) begin
		if (RESET30='0') then
			D30OUT	<=	(others =>'0');
		elsif rising_edge(LE_BS) then 
			D30OUT <= D30_SIG;
		end if;
	end process;

	LATCH_40_R: process (LE_BS, RESET30)
	begin
		if (RESET30='0') then
			D40OUT	<=	(others =>'0');
		elsif (rising_edge(LE_BS)) then

			--Zuordnung und Latch Datenleitungen von 030-BUS zu 040-BUS (READ)
			--BYTE 0
			if(BWL_BS ="000" OR BWL_BS ="100" OR BWL_BS ="110") then
				D40OUT(31 downto 24) <= D30(31 downto 24);
			end if;
			--BYTE 1	
			if(BWL_BS ="100" OR BWL_BS ="110") then
				D40OUT(23 downto 16) <= D30(23 downto 16);
			elsif(BWL_BS ="000" OR BWL_BS ="001") then
				D40OUT(23 downto 16) <= D30(31 downto 24);
			end if;
			--BYTE 2
			if(BWL_BS ="110") then
				D40OUT(15 downto  8) <= D30(15 downto  8);
			elsif(BWL_BS ="000" OR BWL_BS ="001" OR BWL_BS ="010" OR BWL_BS ="100" OR BWL_BS ="101") then
				D40OUT(15 downto  8) <= D30(31 downto 24);
			end if;
			--BYTE 3
			if(BWL_BS ="110") then
				D40OUT( 7 downto  0) <= D30( 7 downto  0);
			elsif(BWL_BS ="000" OR BWL_BS ="001" OR BWL_BS ="010" OR BWL_BS ="011") then
				D40OUT( 7 downto  0) <= D30(31 downto 24);
			elsif(BWL_BS ="100" OR BWL_BS ="101") then
				D40OUT( 7 downto  0) <= D30(23 downto 16);
			end if;						
		end if;
	end process LATCH_40_R;
end Behavioral;

